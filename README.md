**Warning: This software is in development. Expect breaking changes regularly**

# DORA: Dancer's Organizational and Registration Application
[![pipeline status](https://gitlab.com/bfcarpio/dance-organizational-and-registration-application/badges/master/pipeline.svg)](https://gitlab.com/bfcarpio/dance-organizational-and-registration-application/-/commits/master)
[![coverage report](https://gitlab.com/bfcarpio/dance-organizational-and-registration-application/badges/master/coverage.svg)](https://gitlab.com/bfcarpio/dance-organizational-and-registration-application/-/commits/master)
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)

The *Dancer's Organizational and Registration Application* (DORA) is a complete suite to register for, manage, run, and view dance competitions. As I only have experience with a subset of ballroom competitions this software is being written to fit the needs of what I have seen; however, minimal efforts are being made to keep the program generic enough for competitions of other dance styles.

It is written primarily in [Elixir](https://elixir-lang.org/)* and uses the [Postgres](https://www.postgresql.org/) database.

## Why
> This is a big undertaking, why would you try to do this alone for a side project?

After helping run 3 collegiate ballroom competitions I started noticing issues with the curent gold standard of ballroom software, [O2CM](http://www.o2cm.com/). It was slow, the UI sucked, wasn't mobile responsive and hard to maintain since it was written in the 90's in PHP. Elixir is prettier, designed for concurrency, and amazingly stable.

Additionally, this is my attempt to make one giant project that takes everything I have learned over my college career.

> Dora?

I had two partners over the course of 4 years doing collegiate ballroom dance. Gina for 3 years and Dora for the final year. It's really hard to come up with a an acronym with the letters `G`,`I`,`N`, `A`. I also considered calling it `NoToCM` as a joke of `NoSQL` and `O2CM`.

# Development
1. Clone this repository
2. Create a `docker-compose.yml` file with the following:

    ```
    # docker-compose.yml
    version: '3.7'

    services:
      db:
        image: postgres:alpine
        restart: always
        ports:
          - "5432:5432"
        environment:
          - POSTGRES_DB=dora
          - POSTGRES_USER=postgres
          - POSTGRES_PASSWORD=postgres
        volumes:
          - db-data:/var/lib/postgresql/data

    volumes:
      db-data:
    ```
3. You should be able to connect to the database with the username and password noted by `POSTGRES_DB`, `POSTGRES_USER`, and `POSTGRES_PASSWORD` respectively in Dbeaver / your favourite DB manager.
    + You might need to connect to it via `psql --host=localhost -U postgres` and `CREATE DATABASE dora;`
4. `mix deps.get`
5. You should now be able to run `mix` and `mix test`
    + `mix test` uses the DB `dora_test` and should create and migrate it automatically.

# License
+ All contributors must assign all rights to Brendan Carpio.
  + This is merely a formal measure. I'm happy to work with people in good faith and give credit where it's due.
+ AGPL by default but an MIT license is available for purchase


# Footnotes
* Uses some CSS and Javascript for front end stuff. You know, web stuff.
