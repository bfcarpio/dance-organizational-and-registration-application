use Mix.Config

config :master_proxy,
  conn: Plug.Cowboy.Conn

config :master_proxy, MasterProxy.Test.Endpoint,
  http: [port: 4002],
  url: [host: "localhost"],
  secret_key_base: "R832e8CbBikpA2VT50k07PRjmPu9PbDM6ZbNi44s/zVxSSmK1m7H+7Tew4vPwOGX",
  server: false

config :master_proxy,
  http: [:inet6, port: 5907]

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :db, Db.Repo,
  database: "dora_test",
  username: "postgres",
  password: "postgres",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :registrar, Registrar.Endpoint,
  http: [port: 4002],
  server: false

config :logger, level: :info
