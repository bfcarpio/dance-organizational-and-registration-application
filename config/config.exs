use Mix.Config

config :master_proxy,
  http: [:inet6, port: System.get_env("PORT") || 4000],
  backends: [
    %{
      host: ~r/^(registrar.)?.*/,
      phoenix_endpoint: Registrar.Endpoint
    }
  ]

# Configure Mix tasks and generators
config :db,
  ecto_repos: [Db.Repo]

config :phoenix, :json_library, Jason

config :registrar,
  generators: [context_app: false]

# Configures the endpoint
config :registrar, Registrar.Endpoint,
  server: false,
  url: [host: "localhost"],
  secret_key_base: "BciKeIu8E2zBZW0vKsuUGA+hVX/N/lrCtnlpupBiQwtpltnLcgJ3e4qGoV4nKQlF",
  render_errors: [view: Registrar.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Registrar.PubSub,
  live_view: [signing_salt: "m6heri94"]

config :elixir, :time_zone_database, Tzdata.TimeZoneDatabase

import_config "../apps/*/config/config.exs"

import_config "#{Mix.env()}.exs"
