defmodule DORA.MixProject do
  use Mix.Project

  @source_url "https://gitlab.com/bfcarpio/dance-organizational-and-registration-application"

  def project do
    [
      apps_path: "apps",
      version: "0.1.0",
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      source_url: @source_url,
      docs: docs(),
      deps: deps(),
      test_coverage: [tool: ExCoveralls],
      elixirc_paths: elixirc_paths(Mix.env()),
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test
      ],
      dialyzer: [plt_add_apps: [:ex_unit, :mix], ignore_warnings: "config/dialyzer.ignore"]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  defp docs do
    [
      source_ref: "master",
      assets: "docs",
      main: "readme",
      extras: ["README.md"]
    ]
  end

  # Dependencies listed here are available only for this
  # project and cannot be accessed from applications inside
  # the apps folder.
  #
  # Run "mix help deps" for examples and options.
  defp deps do
    [
      {:excoveralls, "~> 0.12.3", only: :test},
      {:ex_doc, "~>0.21.3", branch: "master", only: :dev},
      {:dialyxir, "~> 1.0", only: :dev, runtime: false}
    ]
  end
end
