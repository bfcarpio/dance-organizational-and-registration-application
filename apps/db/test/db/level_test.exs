defmodule Db.LevelTest do
  use Db.DataCase
  import Db.Factory

  alias Db.Level

  @valid_attrs %{name: "Silver"}

  test "changeset with valid attributes" do
    changeset = Level.changeset(%Level{}, @valid_attrs)

    assert changeset.valid?
  end

  test "country code can not be more than 255 letters" do
    attrs = %{name: String.duplicate("X", 256)}
    changeset = Level.changeset(%Level{}, attrs)

    refute changeset.valid?
  end

  test "changeset with different case" do
    insert(:level, @valid_attrs)

    upcase_duplicate = Map.update!(@valid_attrs, :name, &String.upcase/1)
    changeset = Level.changeset(%Level{}, upcase_duplicate)

    # This is true likely because there is no exact match
    # and the case-insensitive key hasn't been computed
    assert changeset.valid?

    # Properly causes an issue here, though!
    {:error, _} = Repo.insert(changeset)

    assert Db.Repo.aggregate(Level, :count) == 1
  end

  test "easy adding a level" do
    {:ok, _} = Level.add("Mambo")

    # Adding again with same name quietly fails
    {:ok, _} = Level.add("Mambo")

    # Adding again with different cased name quietly fails
    {:ok, _} = Level.add("mambo")
  end
end
