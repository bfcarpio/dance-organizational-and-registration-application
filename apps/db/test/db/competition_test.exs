defmodule Db.CompetitionTest do
  use Db.DataCase
  alias Db.Competition

  @valid_attrs %{
    name: "TestCompetition",
    start_datetime: DateTime.add(DateTime.utc_now(), 3600, :second),
    street: "ul. Armii Poznań 123",
    postal_code: "61-663",
    city: "Poznań",
    country: "PL",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Adipiscing diam donec adipiscing tristique."
  }

  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Competition.changeset(%Competition{}, @valid_attrs)

    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Competition.changeset(%Competition{}, @invalid_attrs)

    refute changeset.valid?
  end

  test "start_datetime must be in the future" do
    changeset = Competition.changeset(%Competition{}, %{start_datetime: DateTime.utc_now()})

    refute changeset.valid?
  end

  test "country code cannot be less than 2 letters" do
    attrs = %{@valid_attrs | country: "P"}
    changeset = Competition.changeset(%Competition{}, attrs)

    refute changeset.valid?
  end

  test "country code cannot be more than 2 letters" do
    attrs = %{@valid_attrs | country: "POL"}
    changeset = Competition.changeset(%Competition{}, attrs)

    refute changeset.valid?
  end

  describe "Competition.add()" do
    setup do
      seconds_in_day = 24 * 60 * 60
      future_date = DateTime.add(DateTime.utc_now(), 90 * seconds_in_day)

      %{future_date: future_date}
    end

    test "sucess", context do
      {:ok, _} =
        Competition.add(
          "Name",
          context.future_date,
          "street",
          "postal",
          "city",
          "PL"
        )
    end

    test "sucessfully ignore conflicts", context do
      {:ok, _} =
        Competition.add(
          "Name",
          context.future_date,
          "street",
          "postal",
          "city",
          "PL",
          "description"
        )

      {:ok, _} =
        Competition.add(
          "Name",
          context.future_date,
          "street",
          "postal",
          "city",
          "PL",
          "description",
          "studio"
        )
    end

    test "single arity version" do
      {:ok, _} = Competition.add(@valid_attrs)
    end
  end

  describe "Competition.add_event()" do
    setup do
      insert(:level, %{name: "bronze"})
      insert(:style, %{name: "smooth"})
      insert(:dance, %{name: "tango"})
      competition = insert(:competition)

      %{competition: competition}
    end

    test "Competition.add_event/4", context do
      {:ok, _} = Competition.add_event(context.competition, "bronze", "smooth", "tango")
    end

    test "Competition.add_event/2", context do
      {:ok, _} =
        Competition.add_event(context.competition, %{
          level_name: "bronze",
          style_name: "smooth",
          dance_name: "tango"
        })
    end
  end
end
