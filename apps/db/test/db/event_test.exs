defmodule Db.EventTest do
  use Db.DataCase
  alias Db.{Event, Repo}

  @valid_attrs %{
    level_name: "bronze",
    style_name: "smooth",
    dance_name: "tango",
    competition_id: 1
  }
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Event.changeset(%Event{}, @valid_attrs)

    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Event.changeset(%Event{}, @invalid_attrs)

    refute changeset.valid?
  end

  test "Adding an event" do
    insert(:level, %{name: "bronze"})
    insert(:style, %{name: "smooth"})
    insert(:dance, %{name: "tango"})
    competition = insert(:competition)

    {:ok, _} =
      %Event{}
      |> Event.changeset(%{@valid_attrs | competition_id: competition.id})
      |> Repo.insert(on_conflict: :nothing)
  end
end
