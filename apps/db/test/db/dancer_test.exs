defmodule Db.DancerTest do
  use Db.DataCase

  alias Db.Dancer

  @valid_attrs %{
    first_name: "John",
    last_name: "Smith",
    country: "US",
    studio: "MSUBDT"
  }
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Dancer.changeset(%Dancer{}, @valid_attrs)

    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Dancer.changeset(%Dancer{}, @invalid_attrs)

    refute changeset.valid?
  end

  describe "Dancer.country" do
    test "cannot be less than 2 letters" do
      attrs = %{@valid_attrs | country: "P"}
      changeset = Dancer.changeset(%Dancer{}, attrs)

      refute changeset.valid?
    end

    test "cannot be more than 2 letters" do
      attrs = %{@valid_attrs | country: "POL"}
      changeset = Dancer.changeset(%Dancer{}, attrs)

      refute changeset.valid?
    end
  end

  describe "Dancer.add()" do
    test "Dancer.add/3" do
      {:ok, _} = Dancer.add("John", "Smith", "US")
    end

    test "Dancer.add/4" do
      {:ok, _} = Dancer.add("John", "Smith", "US", "MSUBDT")
    end

    test "Dancer.add/1" do
      {:ok, _} = Dancer.add(@valid_attrs)
    end
  end
end
