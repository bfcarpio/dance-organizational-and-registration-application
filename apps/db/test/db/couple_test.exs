defmodule Db.CoupleTest do
  use Db.DataCase

  alias Db.{Couple, Event, Dancer}

  describe "Couple.add() with existing dancers and events" do
    setup do
      lead = insert(:dancer, %{first_name: "Arnold"})
      follow = insert(:dancer, %{first_name: "Betty"})

      insert(:level)
      insert(:style)
      insert(:dance)

      competition = insert(:competition)
      event = insert(:event, %{competition_id: competition.id})

      %{event: event, lead: lead, follow: follow}
    end

    test "sucess", context do
      {:ok, _} = Couple.add(context.event, context.lead, context.follow)
    end

    test "sucessfully ignore conflicts", context do
      {:ok, _} = Couple.add(context.event, context.lead, context.follow)
      {:ok, _} = Couple.add(context.event, context.lead, context.follow)
    end
  end

  describe "Couple.add() without existing dancers and events" do
    test "error" do
      # These IDs don't exist
      {:error, _} = Couple.add(%Event{id: -1}, %Dancer{id: -1}, %Dancer{id: -2})
    end
  end
end
