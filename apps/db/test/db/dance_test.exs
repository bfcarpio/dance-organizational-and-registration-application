defmodule Db.DanceTest do
  use Db.DataCase
  import Db.Factory

  alias Db.Dance

  @valid_attrs %{name: "test"}

  test "changeset with valid attributes" do
    changeset = Dance.changeset(%Dance{}, @valid_attrs)

    assert changeset.valid?
  end

  test "country code can not be more than 255 letters" do
    attrs = %{name: String.duplicate("X", 256)}
    changeset = Dance.changeset(%Dance{}, attrs)

    refute changeset.valid?
  end

  test "changeset with different case" do
    prev_count = Db.Repo.aggregate(Dance, :count)

    insert(:dance, @valid_attrs)

    upcase_duplicate = Map.update!(@valid_attrs, :name, &String.upcase/1)
    changeset = Dance.changeset(%Dance{}, upcase_duplicate)

    # This is true likely because there is no exact match
    # and the case-insensitive key hasn't been computed
    assert changeset.valid?

    # Properly causes an issue here, though!
    # We want this to occur because then we know
    # it conflicted with the lower case option
    {:error, _} = Repo.insert(changeset)

    assert Db.Repo.aggregate(Dance, :count) == prev_count + 1
  end

  test "easy adding a dance" do
    {:ok, _} = Dance.add("Mambo")

    # Adding again with same name quietly fails
    {:ok, _} = Dance.add("Mambo")

    # Adding again with different cased name quietly fails
    {:ok, _} = Dance.add("mambo")
  end
end
