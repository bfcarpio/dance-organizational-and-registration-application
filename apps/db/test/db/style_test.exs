defmodule Db.StyleTest do
  use Db.DataCase
  import Db.Factory

  alias Db.Style

  @valid_attrs %{name: "Standard"}

  test "changeset with valid attributes" do
    changeset = Style.changeset(%Style{}, @valid_attrs)

    assert changeset.valid?
  end

  test "country code can not be more than 255 letters" do
    attrs = %{name: String.duplicate("X", 256)}
    changeset = Style.changeset(%Style{}, attrs)

    refute changeset.valid?
  end

  test "changeset with different case" do
    insert(:style, @valid_attrs)

    upcase_duplicate = Map.update!(@valid_attrs, :name, &String.upcase/1)
    changeset = Style.changeset(%Style{}, upcase_duplicate)

    # This is true likely because there is no exact match
    # and the case-insensitive key hasn't been computed
    assert changeset.valid?

    # Properly causes an issue here, though!
    {:error, _} = Repo.insert(changeset)

    assert Db.Repo.aggregate(Style, :count) == 1
  end

  test "easy adding a style" do
    {:ok, _} = Style.add("Standard")
    #
    # Adding again with same name quietly fails
    {:ok, _} = Style.add("Standard")

    # Adding again with different cased name quietly fails
    {:ok, _} = Style.add("standard")
  end
end
