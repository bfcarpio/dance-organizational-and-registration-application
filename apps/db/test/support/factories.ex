defmodule Db.Factory do
  @moduledoc false
  use ExMachina.Ecto, repo: Db.Repo

  def dancer_factory do
    %Db.Dancer{
      first_name: "Bronisław",
      last_name: "Jabłoński",
      country: "PL",
      studio: "The Best Dance Studio.pl"
    }
  end

  def competition_factory do
    %Db.Competition{
      name: "Test Competition 2020",
      start_datetime: DateTime.add(DateTime.utc_now(), 3600, :second),
      street: "ul. Bliźniąt 130",
      postal_code: "61-244",
      city: "Poznań",
      country: "PL",
      description:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Adipiscing diam donec adipiscing tristique."
    }
  end

  def level_factory do
    %Db.Level{
      name: "Bronze"
    }
  end

  def style_factory do
    %Db.Style{
      name: "Smooth"
    }
  end

  def dance_factory do
    %Db.Dance{
      name: "Tango"
    }
  end

  def event_factory do
    %Db.Event{
      level_name: "bronze",
      style_name: "smooth",
      dance_name: "tango",
      competition_id: 1
    }
  end
end
