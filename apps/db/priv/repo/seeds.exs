# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Db.Repo.insert!(%Db.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Db.{Style, Dance, Dancer, Level, Competition, Couple}

levels = ~w(newcomer bronze silver gold)
Enum.each(levels, &Level.add/1)

styles = ~w(smooth standard rhythm latin)
Enum.each(styles, &Style.add/1)

dances = ~w(waltz tango foxtrot rhumba)
Enum.each(dances, &Dance.add/1)

{:ok, competition} =
  Competition.add(%{
    name: "Green and White Gala",
    start_datetime: DateTime.add(DateTime.utc_now(), 3600, :second),
    street: "229 Dem Hall Rd",
    postal_code: "48824",
    city: "East Lansing",
    country: "US"
  })

{:ok, silver_standard_waltz} = Competition.add_event(competition, "silver", "standard", "waltz")

{:ok, _} =
  Competition.add(%{
    name: "Irish Gala",
    start_datetime: DateTime.add(DateTime.utc_now(), 3600 * 2, :second),
    street: "Irish Lane",
    postal_code: "3456",
    city: "Not here",
    country: "US"
  })

{:ok, anthony} = Dancer.add("Anthony", "Albatross", "US")
{:ok, bianca} = Dancer.add("Bianca", "Badger", "US")

Couple.add(silver_standard_waltz, anthony, bianca)
