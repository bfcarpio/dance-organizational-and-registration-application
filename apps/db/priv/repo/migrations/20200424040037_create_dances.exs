defmodule Db.Repo.Migrations.CreateDances do
  use Ecto.Migration

  def change do
    create table(:dances, primary_key: false) do
      add(:name, :citext, primary_key: true)

      timestamps(type: :timestamptz)
    end
  end
end
