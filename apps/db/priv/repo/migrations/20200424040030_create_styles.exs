defmodule Db.Repo.Migrations.CreateStyles do
  use Ecto.Migration

  def change do
    create table(:styles, primary_key: false) do
      add(:name, :citext, primary_key: true)

      timestamps(type: :timestamptz)
    end
  end
end
