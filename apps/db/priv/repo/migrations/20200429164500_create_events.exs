defmodule Db.Repo.Migrations.CreateEvents do
  use Ecto.Migration

  def change do
    create table(:events) do
      add(:level_name, references(:levels, type: :citext, column: :name), null: false)
      add(:style_name, references(:styles, type: :citext, column: :name), null: false)
      add(:dance_name, references(:dances, type: :citext, column: :name), null: false)
      add(:competition_id, references(:competitions, on_delete: :delete_all), null: false)
      add(:order, :smallint)

      timestamps(type: :timestamptz)
    end

    create(
      unique_index(:events, [:level_name, :style_name, :dance_name, :competition_id],
        name: :unique_event
      )
    )

    create(index(:events, [:competition_id, :order]))
  end
end
