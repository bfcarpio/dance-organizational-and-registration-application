defmodule Db.Repo.Migrations.CreateCouples do
  use Ecto.Migration

  def change do
    create table(:couples) do
      add(:event_id, references(:events, on_delete: :delete_all))
      add(:lead_id, references(:dancers, on_delete: :delete_all))
      add(:follow_id, references(:dancers, on_delete: :delete_all))
      timestamps(type: :timestamptz)
    end

    create(index(:couples, [:event_id]))
  end
end
