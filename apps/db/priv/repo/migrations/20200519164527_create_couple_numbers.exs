defmodule Db.Repo.Migrations.CreateCoupleNumbers do
  use Ecto.Migration

  def change do
    create table(:couple_numbers, primary_key: false) do
      add(:competition_id, references(:competitions, on_delete: :delete_all), primary_key: true)
      add(:lead_id, references(:dancers, on_delete: :delete_all), primary_key: true)
      add(:number, :smallint, null: false)
      timestamps(type: :timestamptz)
    end

    create(index(:couple_numbers, [:competition_id, :number]))

    create(constraint(:couple_numbers, :number_must_be_positive, check: "number > 0"))
  end
end
