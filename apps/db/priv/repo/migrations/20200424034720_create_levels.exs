defmodule Db.Repo.Migrations.CreateLevels do
  use Ecto.Migration

  def change do
    create table(:levels, primary_key: false) do
      add(:name, :citext, primary_key: true)

      timestamps(type: :timestamptz)
    end
  end
end
