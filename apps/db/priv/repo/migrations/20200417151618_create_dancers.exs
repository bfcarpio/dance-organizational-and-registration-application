defmodule Db.Repo.Migrations.CreateDancers do
  use Ecto.Migration

  def change do
    create table(:dancers) do
      add(:first_name, :string, null: false)
      add(:last_name, :string, null: false)
      add(:country, :"char(2)", null: false)
      add(:studio, :string, default: "", null: false)

      timestamps(type: :timestamptz)
    end
  end
end
