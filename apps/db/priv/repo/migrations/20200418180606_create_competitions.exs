defmodule Db.Repo.Migrations.CreateCompetitions do
  use Ecto.Migration

  def change do
    create table(:competitions) do
      add(:name, :string, null: false)
      add(:start_datetime, :timestamptz, null: false)
      add(:street, :string, null: false)
      add(:postal_code, :string, null: false)
      add(:city, :string, null: false)
      add(:country, :"char(2)", null: false)
      add(:description, :string, default: "", null: false)
      add(:studio, :string, default: "", null: false)

      timestamps(type: :timestamptz)
    end

    create(index(:competitions, ["start_datetime DESC"]))
  end
end
