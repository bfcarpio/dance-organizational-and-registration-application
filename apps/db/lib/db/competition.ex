defmodule Db.Competition do
  @moduledoc """
  Competition entity modeled from the database

  Competition should:
  + Take place in a location have have a concrete starting time
  + Have multiple events
  + Have couples take part in events

  """
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query
  alias Db.{Competition, Event, Repo}

  schema "competitions" do
    field(:name, :string)
    field(:start_datetime, :utc_datetime)
    field(:street, :string)
    field(:postal_code, :string)
    field(:city, :string)
    field(:country, :string)
    field(:description, :string, default: "")
    field(:studio, :string, default: "", null: false)

    has_many(:events, Db.Event)

    timestamps(type: :utc_datetime)
  end

  @required [:name, :start_datetime, :street, :postal_code, :city, :country]
  @fields List.flatten([[:studio, :description], @required])

  @iso3166 "must be in ISO 3166 alpha-2 format (two characters uppercased)"

  @doc false
  @spec changeset(map(), map()) :: %Ecto.Changeset{}
  def changeset(competition, params \\ %{}) do
    competition
    |> cast(params, @fields)
    |> validate_required(@required)
    |> validate_length(:country,
      is: 2,
      message: @iso3166
    )
    |> validate_format(:country, ~r/[A-Z]{2}/, message: @iso3166)
    |> validate_change(:start_datetime, fn :start_datetime, sdt ->
      case DateTime.compare(sdt, DateTime.utc_now()) do
        :gt ->
          []

        _ ->
          [start_datetime: "must be in the future"]
      end
    end)
  end

  @spec add(
          String.t(),
          String.t(),
          String.t(),
          String.t(),
          String.t(),
          String.t(),
          String.t(),
          String.t()
        ) :: {:ok, map()} | {:error, map()}
  def add(
        name,
        start_datetime,
        street,
        postal_code,
        city,
        country,
        description \\ "",
        studio \\ ""
      ) do
    add(%{
      name: name,
      start_datetime: start_datetime,
      street: street,
      postal_code: postal_code,
      city: city,
      country: country,
      description: description,
      studio: studio
    })
  end

  @spec add(map()) :: {:ok, map()} | {:error, map()}
  def add(%{name: _, start_datetime: _, street: _, postal_code: _, city: _, country: _} = attrs)
      when is_map(attrs) do
    %Competition{}
    |> changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Adds an event to the competition.
  """
  @spec add_event(
          %Competition{id: non_neg_integer()},
          String.t(),
          String.t(),
          String.t(),
          nil | non_neg_integer()
        ) :: {:ok, map()} | {:error, map()}
  def add_event(%Competition{id: _} = competition, level, style, dance, order \\ nil)
      when is_map(competition) do
    add_event(competition, %{
      level_name: level,
      style_name: style,
      dance_name: dance,
      order: order
    })
  end

  @spec add_event(%Competition{id: non_neg_integer()}, map()) ::
          {:ok, map()} | {:error, map()}
  def add_event(
        %Competition{id: _} = competition,
        %{level_name: _, style_name: _, dance_name: _} = attrs
      )
      when is_map(attrs) and is_map(competition) do
    event =
      competition
      |> Ecto.build_assoc(:events, attrs)

    %Event{}
    |> Event.changeset(Map.from_struct(event))
    |> Repo.insert(on_conflict: :nothing)
  end

  @spec order_by_start_datetime(any()) :: any()
  def order_by_start_datetime(query \\ Competition) do
    from(c in query,
      order_by: [desc: :start_datetime]
    )
  end
end
