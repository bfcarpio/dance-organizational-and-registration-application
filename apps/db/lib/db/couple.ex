defmodule Db.Couple do
  @moduledoc """
  A realtion of 2 dancers (a lead and a follow) and an event.

  Dancers may be apart of many couples and may be in either of the two roles.

  However, dancers may not be in an event more than once regardless of couple and or role.
  """
  use Ecto.Schema
  import Ecto.Changeset
  alias Db.{CoupleNumbers, Competition, Couple, Repo, Event, Dancer}

  schema "couples" do
    belongs_to(:event, Event)
    belongs_to(:lead, Dancer)
    belongs_to(:follow, Dancer)

    has_one(:competition, through: [:event, :competition])

    timestamps(type: :utc_datetime)
  end

  @fields [:event_id, :lead_id, :follow_id]

  @spec changeset(map(), map()) :: %Ecto.Changeset{}
  def changeset(couple, params \\ %{}) do
    couple
    |> cast(params, @fields)
    |> validate_required(@fields)
    |> assoc_constraint(:event)
    |> assoc_constraint(:lead)
    |> assoc_constraint(:follow)
  end

  @spec add(%Event{}, %Dancer{}, %Dancer{}) :: {:ok, map()} | {:error, map()}
  def add(%Event{id: _} = event, %Dancer{id: _} = lead, %Dancer{id: _} = follow)
      when is_map(event) and is_map(lead) and is_map(follow) do
    couple =
      event
      |> Ecto.build_assoc(:couples, %{lead_id: lead.id, follow_id: follow.id})

    %Couple{}
    |> Couple.changeset(Map.from_struct(couple))
    |> Repo.insert()
  end

  @spec add(%Couple{}, %Competition{}, non_neg_integer()) ::
          {:ok, map()} | {:error, map()}
  defdelegate assign_number(couple, competition, number), to: CoupleNumbers, as: :add
end
