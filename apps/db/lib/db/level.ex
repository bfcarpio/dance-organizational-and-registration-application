defmodule Db.Level do
  @moduledoc false

  use Ecto.Schema
  import Ecto.Changeset
  alias Db.{Repo, Event, Level}

  @primary_key {:name, :string, autogenerate: false}
  schema "levels" do
    has_many(:events, Event)

    timestamps(type: :utc_datetime)
  end

  @doc false
  @spec changeset(map(), map()) :: %Ecto.Changeset{}
  def changeset(%Level{} = level, params \\ %{}) do
    level
    |> cast(params, [:name])
    |> validate_required([:name])
    |> validate_length(:name, max: 255)
    |> unique_constraint(:name, name: :levels_pkey)
  end

  @doc """
  Adds the lowercased level to the database

  Clients are expected to "re-case" and format the names appropriately for their display.
  """
  @spec add(String.t()) :: {:ok, %Level{}} | {:error, map()}
  def add(name) do
    %Level{}
    |> changeset(%{name: name})
    |> Repo.insert(on_conflict: :nothing)
  end
end
