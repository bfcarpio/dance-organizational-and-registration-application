defmodule Db.Style do
  @moduledoc false

  use Ecto.Schema
  import Ecto.Changeset
  alias Db.{Repo, Event, Style}

  @primary_key {:name, :string, autogenerate: false}
  schema "styles" do
    has_many(:events, Event)

    timestamps(type: :utc_datetime)
  end

  @doc false
  @spec changeset(map(), map()) :: %Ecto.Changeset{}
  def changeset(style, params \\ %{}) do
    style
    |> cast(params, [:name])
    |> validate_required([:name])
    |> validate_length(:name, max: 255)
    |> unique_constraint(:name, name: :styles_pkey)
  end

  @doc """
  Adds the lowercased style to the database

  Clients are expected to "re-case" and format the names appropriately for their display.
  """
  @spec add(String.t()) :: {:ok, %Style{}} | {:error, map()}
  def add(name) do
    %Style{}
    |> changeset(%{name: name})
    |> Repo.insert(on_conflict: :nothing)
  end
end
