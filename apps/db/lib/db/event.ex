defmodule Db.Event do
  @moduledoc """
  Event entity modeled from the database

  Events should
  + Be composed of a unique combination of levels, styles, and dances.
  + Take place at a competition in some order
  + Be joinable by a dancer
  """
  use Ecto.Schema
  import Ecto.Changeset
  alias Db.{Event, Level, Style, Dance, Competition}

  schema "events" do
    belongs_to(:level, Level, type: :string, references: :name, foreign_key: :level_name)
    belongs_to(:style, Style, type: :string, references: :name, foreign_key: :style_name)
    belongs_to(:dance, Dance, type: :string, references: :name, foreign_key: :dance_name)
    belongs_to(:competition, Competition)
    field(:order, :integer)

    has_many(:couples, Db.Couple)

    timestamps(type: :utc_datetime)
  end

  @unique_fields [:level_name, :style_name, :dance_name, :competition_id]

  @spec changeset(%Event{}, map()) :: %Ecto.Changeset{}
  def changeset(event, params \\ %{}) do
    event
    |> cast(params, [:order | @unique_fields])
    |> validate_required(@unique_fields)
    |> assoc_constraint(:level)
    |> assoc_constraint(:style)
    |> assoc_constraint(:dance)
    |> assoc_constraint(:competition)
    |> unique_constraint(@unique_fields, name: :unique_event)
  end
end
