defmodule Db.Dancer do
  @moduledoc """
  Dancer entity modeled from the database.

  Dancers should:
  + Create a couple with another dancer
  + Be either a lead or follow
    + If they are a lead then they should get a unique number at each competition
  + Take part in events at each competition.
    + They cannot be in an event more than once
  """
  use Ecto.Schema
  import Ecto.Changeset
  alias Db.{Dancer, Repo}

  schema "dancers" do
    field(:first_name, :string)
    field(:last_name, :string)
    field(:country, :string)
    field(:studio, :string)

    has_many(:couples_led, Db.Couple, foreign_key: :lead_id)
    has_many(:couples_followed, Db.Couple, foreign_key: :follow_id)

    timestamps(type: :utc_datetime)
  end

  @required_fields [:first_name, :last_name, :country]
  @fields [:studio | @required_fields]

  @iso3166 "must be in ISO 3166 alpha-2 format (two characters uppercased)"

  @doc false
  @spec changeset(map(), map()) :: %Ecto.Changeset{}
  def changeset(dancer, params \\ %{}) do
    dancer
    |> cast(params, @fields)
    |> validate_required(@required_fields)
    |> validate_length(:country,
      is: 2,
      message: @iso3166
    )
    |> validate_format(:country, ~r/[A-Z]{2}/, message: @iso3166)
  end

  @spec add(String.t(), String.t(), String.t(), String.t()) :: {:ok, map()} | {:error, map()}
  def add(first_name, last_name, country, studio \\ "") do
    add(%{first_name: first_name, last_name: last_name, country: country, studio: studio})
  end

  @spec add(map()) :: {:ok, map()} | {:error, map()}
  def add(attrs) when is_map(attrs) do
    %Dancer{}
    |> changeset(attrs)
    |> Repo.insert()
  end
end
