defmodule Db.Dance do
  @moduledoc false

  use Ecto.Schema
  import Ecto.Changeset
  alias Db.{Repo, Event, Dance}

  @primary_key {:name, :string, autogenerate: false}
  schema "dances" do
    has_many(:events, Event)

    timestamps(type: :utc_datetime)
  end

  @doc """
  Checks the following conditions:
  + A string `:id` has been provided
  + The string is <= 100 characters long
  + Case is NOT preserved
  """
  @spec changeset(map(), map()) :: %Ecto.Changeset{}
  def changeset(dance, params \\ %{}) do
    dance
    |> cast(params, [:name])
    |> validate_required([:name])
    |> validate_length(:name, max: 255)
    |> unique_constraint(:name, name: :dances_pkey)
  end

  @doc """
  Adds a case-insensitive dance to the database

  Clients are expected to "re-case" and format the names appropriately for their display.
  """
  @spec add(String.t()) :: {:ok, %Dance{}} | {:error, map()}
  def add(name) do
    %Dance{}
    |> changeset(%{name: name})
    |> Repo.insert(on_conflict: :nothing)
  end
end
