defmodule Db.CoupleNumbers do
  @moduledoc """
  A relation of a lead (dancer) and an competition to a number.

  This number represents the one that is pinned to a dancer's back during the competiton.

  Should be created in large batches, but can be individually updated to make fixes.

  Avoid working with this module as much as possible. Functionality directly involving `CoupleNumbers` can be implemented here, but accessed with a delegation via a more appropriate module, such as `Couple`.
  """
  use Ecto.Schema
  import Ecto.Changeset
  alias Db.{CoupleNumbers, Competition, Couple, Dancer, Repo}

  @primary_key false
  schema "couple_numbers" do
    belongs_to(:competition, Competition, primary_key: true)
    belongs_to(:lead, Dancer, primary_key: true)
    field(:number, :integer)

    timestamps(type: :utc_datetime)
  end

  @fields [:competition_id, :lead_id, :number]

  @spec changeset(%CoupleNumbers{}, map()) :: %Ecto.Changeset{}
  def changeset(cn, params \\ %{}) do
    cn
    |> cast(params, @fields)
    |> validate_required(@fields)
    |> assoc_constraint(:competition)
    |> assoc_constraint(:lead)
    |> check_constraint(:number, name: :number_must_be_positive)
  end

  @spec add(%Couple{}, %Competition{}, non_neg_integer()) ::
          {:ok, map()} | {:error, map()}
  def add(%Couple{lead_id: lead_id}, %Competition{id: competition_id}, number) do
    %CoupleNumbers{}
    |> changeset(%{lead_id: lead_id, competition_id: competition_id, number: number})
    |> Repo.insert(returning: :number)
  end
end
